
COMMENT !
	; byte[] pixelsIn, byte[] pixelsOut, int w, int hsp, float scale, int input_stride, int output_stride, int outputWidth

	int index1, index2;
	int j, k;
	float x, y;
	uint8_t t;
	index2 = hsp * output_stride;
	int outputWidth = w * scale;
	for (j = 0; j < outputWidth; j++) //column
	{
		x = ((float)(j)) / scale;
		index1 = (int)(x) << 2;
		x -= (int)(x);
		y = ((float)(hsp)) / scale;
		index1 += (int)(y)*input_stride;
		y -= (int)(y);
		for (k = 0; k < 4; k++) //for color in R, G, B, A
		{
			t = (pixelsIn[index1]) * (1.0 - x) * (1.0 - y);
			t += (pixelsIn[index1 + 4]) * (x) * (1.0 - y);
			t += (pixelsIn[index1 + w << 2]) * (1.0 - x) * (y);
			t += (pixelsIn[index1 + w << 2 + 4]) * (x) * (y);
			pixelsOut[index2] = (t);
			index1++;
			index2++;
		}
	} //column
!

.data
	w QWORD ?
	scale QWORD ?
	is QWORD ?
	os QWORD ?
	ow QWORD ?
	shuffle1 db 0,4,8,12,1,1,1,1,1,1,1,1,1,1,1,1

.code

SetParametersASM proc ; int w, int scale, int is, int os
	mov w, RCX ; RCX zawiera warto�� w
	mov scale, RDX ; RDX zawiera warto�� scale

	mov is, R8 ; R8 zawiera warto�� is
	mov os, R9 ; R9 zawiera warto�� os
	
	;  int outputWidth = w * scale;
	movq MM0, w ; przesuwamy warto�� w do rejestru MM0
	movq MM1, scale ; przesuwamy warto�� scale do rejestru MM1
	PMULUDQ MM0, MM1 ; Mno�enie zawarto�ci rejestr�w MM0 i MM1, zapis do MM0
	movq ow, MM0 ; Przeniesienie warto��i rejestr MM0, do zmiennej globalnej ow

	ret

SetParametersASM endp

ScalingASM proc

	PUSH R10
	PUSH R11
	PUSH R12
	PUSH R13
	PUSH R14
	PUSH R15

	; RCX - byte[] source
	; RDX - byte[] target
	; R8 - int h

	mov R12, 1


	; index2 = hsp * output_stride;
	; MM6
	; NOTE: h in MM7
	movq MM7, R8 ; przesuwamy warto�� h do MM7
	movq MM6, os ; przesuwamy output stride to MM6
	pmuludq MM6, MM7 ; Mno�enie zawarto�ci rejestr�w MM6 i MM7, zapis do MM6

	; y = h / scale;
	; XMM0
	cvtsi2ss xmm0, R8
	cvtpi2ps xmm5, scale
	divss xmm0, xmm5 ; wynik w xmm0
	movss xmm1, xmm0
	cvttps2pi mm3, xmm0


	; index1 = (int)(y)*input_stride;
	; MM7
	movq MM7, is
	pmuludq MM7, MM3
	
	; int y1 = y - (int)y
	; XMM1
	cvtpi2ps xmm4, mm3
	subss xmm1, xmm4 ; wynik w xmm1

	; licznik p�tli
	mov R14, 0
loopers:
	movups xmm3, [rdx]

	; x = ((float)(j)) / scale;
	; XMM2
	cvtsi2ss xmm2, R14
	cvtpi2ps xmm5, scale
	divss xmm2, xmm5 ; x -> wynik w xmm2

	inc R14 ; zwi�ksz licznik


	; index1 = (int)(x) * 4;
	; MM5
	cvtps2pi mm5, xmm2
	PSLLD mm5, 2
	paddd MM5, MM7

	; x -= (int)(x);
	; XMM2
	roundps xmm5, xmm2, 11B
	subss xmm2, xmm5 ; wynik w xmm2

	; pixelIn offset
	movq r11, mm5
	mov rax,rcx
	add rax,r11

	; pixelIn
	movups xmm3, [rax]
	PMOVZXBD xmm3, xmm3
	VCVTUDQ2PS xmm3, xmm3

	; 1.0 - x
	; XMM7
	cvtsi2ss xmm7, r12 ; 1.0
	subss xmm7, xmm2
	shufps xmm7, xmm7, 0h

	; 1.0 - y1
	; XMM5
	cvtsi2ss xmm5, r12 ; 1.0
	subss xmm5, xmm1
	shufps xmm5, xmm5, 0h

	xorps xmm4, xmm4
	mulps xmm3, xmm7
	mulps xmm3, xmm5
	addps xmm4, xmm3

	; Next
	;
	;

	; pixelIn
	movups xmm3, [rax+4]
	PMOVZXBD xmm3, xmm3
	VCVTUDQ2PS xmm3, xmm3

	; x
	; XMM6
	movss xmm6, xmm2
	shufps xmm6, xmm6, 0h

	mulps xmm3, xmm6
	mulps xmm3, xmm5
	addps xmm4, xmm3

	; Next
	;
	;
	mov rdi, w

	; pixelIn
	movups xmm3, [rax+4*rdi]
	PMOVZXBD xmm3, xmm3
	VCVTUDQ2PS xmm3, xmm3

	; y
	; XMM5
	movss xmm5, xmm1
	shufps xmm5, xmm5, 0h

	mulps xmm3, xmm7
	mulps xmm3, xmm5
	addps xmm4, xmm3

	movups xmm3, [rax+4+4*rdi]
	PMOVZXBD xmm3, xmm3
	VCVTUDQ2PS xmm3, xmm3

	mulps xmm3, xmm6
	mulps xmm3, xmm5
	addps xmm4, xmm3

	VCVTPS2UDQ xmm4, xmm4
	MOVUPS XMM3, XMMWORD PTR [shuffle1]
	PSHUFB xmm4, xmm3

	movq r10, MM6
	movdqu [rdx+r10], xmm4
	
	add R10, 4
	movq MM6, R10
	movups xmm3, [rdx]



	CMP R14, ow
	JNZ loopers

	POP R15
	POP R14
	POP R13
	POP R12
	POP R11
	POP R10
	ret

ScalingASM endp

end