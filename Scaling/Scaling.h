#pragma once
#include <xatomic.h>
#ifdef SCALING_EXPORTS
#define SCALING_API __declspec(dllexport)
#else
#define SCALING_API __declspec(dllimport)
#endif

extern "C" SCALING_API void ScalingC(uint8_t * pixelsIn, uint8_t * pixelsOut, int hsp);
extern "C" SCALING_API void SetParametersC(int w1, int s, int is, int os);