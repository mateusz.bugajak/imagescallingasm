#include "pch.h"
#include "Scaling.h"
#include <stdint.h>
#include <iostream>

int w;
int scale;
int input_stride;
int output_stride;

void SetParametersC(const int w1, const int s, const int is, const int os) {
	w = w1;
	scale = s;
	input_stride = is;
	output_stride = os;
}

void ScalingC(uint8_t* pixelsIn, uint8_t* pixelsOut, const int hsp)
{
	int index1, index2;
	int j, k;
	float x, y;
	uint8_t t;
	index2 = hsp * output_stride;
	int outputWidth = w * scale; 
	for (j = 0; j < outputWidth; j++) 
	{
		x = ((float)(j)) / scale; 
		index1 = (int)(x) * 4;  
		x -= (int)(x); 
		y = ((float)(hsp)) / scale; 
		index1 += (int)(y)*input_stride; 
		y -= (int)(y);  
		auto a = (pixelsIn[index1]);
		auto b = (float)(pixelsIn[index1 + 4]);
		auto c = (float)(pixelsIn[index1 + w * 4]);
		auto d = (float)(pixelsIn[index1 + w * 4 + 4]);
		for (k = 0; k < 4; k++) //for color in R, G, B, A
		{
			t = (float)(pixelsIn[index1]) * (1.0 - x) * (1.0 - y);
			t += (float)(pixelsIn[index1 + 4]) * (x) * (1.0 - y);
			t += (float)(pixelsIn[index1 + w * 4]) * (1.0 - x) * (y);
			t += (float)(pixelsIn[index1 + w * 4 + 4]) * (x) * (y); 
			pixelsOut[index2] = t;
			index1++;
			index2++;
		}
	} //column
} //line