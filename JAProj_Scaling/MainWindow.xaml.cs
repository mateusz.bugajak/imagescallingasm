﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Image = System.Drawing.Image;
using Rectangle = System.Drawing.Rectangle;

namespace JAProj_Scaling
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Uri imagePath;
        BitmapImage image;
        int threads;
        bool asm = true;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ImgBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                String path = openFileDialog.FileName;
                PathBox.Text = path;
                imagePath = new Uri(path);
                checkPathAndAlg();
                prepareBitmap();
            }
        }

        private void prepareBitmap()
        {
            BitmapImage bitmapImg = new BitmapImage(imagePath);
            bitmapImg.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImg.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            imgView.Source = bitmapImg;
            image = bitmapImg;
        }

        private void algCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            checkPathAndAlg();
            asm = algCB.SelectedIndex == 1 ? true : false;
        }

        private void checkPathAndAlg()
        {
            if (imagePath != null && algCB.SelectedItem != null)
            {
                RunBtn.IsEnabled = true;
            }
        }

        private Bitmap convertBitmapImageToBitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BitmapEncoder bitmapEncoder = new BmpBitmapEncoder();
                bitmapEncoder.Frames.Add(
                    BitmapFrame.Create(bitmapImage)
                );
                bitmapEncoder.Save(memoryStream);

                Bitmap notConverted = new Bitmap(memoryStream);

                return ConvertTo32bpp(notConverted);
            }
        }

        public static Bitmap ConvertTo32bpp(Image img)
        {
            var bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppRgb);
            using (var gr = Graphics.FromImage(bmp))
                gr.DrawImage(img, new Rectangle(0, 0, img.Width, img.Height));
            return bmp;
        }

        private void RunBtn_Click(object sender, RoutedEventArgs e)
        {
            int.TryParse(scale1.Text, out int numValue);
            int scale = numValue != 0 ? numValue : 1;
            Bitmap bitmap = convertBitmapImageToBitmap(image);
            Bitmap newBitmap = new(bitmap.Width * scale, bitmap.Height * scale, PixelFormat.Format32bppRgb);
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            Rectangle rect2 = new Rectangle(0, 0, newBitmap.Width, newBitmap.Height);

            BitmapData data = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);
            BitmapData data2 = newBitmap.LockBits(rect2, ImageLockMode.ReadWrite, newBitmap.PixelFormat);
            int depth = Bitmap.GetPixelFormatSize(data.PixelFormat) / 8; //bytes per pixel
            int depth2 = Bitmap.GetPixelFormatSize(data2.PixelFormat) / 8; //bytes per pixel

            int offset = data2.Width * data2.Height * depth2;
            byte[] buffer = new byte[data.Stride * data.Height * depth];
            byte[] buffer2 = new byte[data2.Width * data2.Height * depth2];

            //copy pixels to buffer
            Marshal.Copy(data.Scan0, buffer, 0, data.Width * data.Height * depth);
            TimeSpan a;
            if (asm == true)
            {
                a = Asm(data, data2, scale, buffer, buffer2);
            }
            else
            {
                a = Cpp(data, data2, scale, buffer, buffer2);
            }

            tim.Text = a.TotalMilliseconds.ToString();

            //Copy the buffer back to image
            Marshal.Copy(buffer2, 0, data2.Scan0, offset);

            newBitmap.UnlockBits(data2);
            //imgView.Source = null;
            imgView.Source = Bitmap2BitmapImage(newBitmap);
            newBitmap.Save("test1.bmp");
        }
        private TimeSpan Cpp(BitmapData data, BitmapData data2, int scale, byte[] buffer, byte[] buffer2)
        {
            var timer = new Stopwatch();
            timer.Start();
            SetParametersC(data.Width, scale, data.Stride, data2.Stride);
            Parallel.For(0, data2.Height, new ParallelOptions { MaxDegreeOfParallelism = threads }, i =>
            {
                ScalingC(
                    buffer,
                    buffer2,
                    i
                );
            });
            timer.Stop();
            return timer.Elapsed;
        }

        private TimeSpan Asm(BitmapData data, BitmapData data2, int scale, byte[] buffer, byte[] buffer2)
        {
            var timer = new Stopwatch();
            timer.Start();
            SetParametersASM(data.Width, scale, data.Stride, data2.Stride);
            Parallel.For(0, data2.Height, new ParallelOptions { MaxDegreeOfParallelism = threads }, i =>
            {
                ScalingASM(
                    buffer,
                    buffer2,
                    i
                );
            });
            timer.Stop();
            return timer.Elapsed;
        }

        [DllImport(@"C:\Users\bugaj\OneDrive\Pulpit\JA\JAProj_Scaling\x64\Release\Scaling.dll")]
        private static extern void SetParametersC(int w1, int w2, int h1, int h2);

        [DllImport(@"C:\\Users\\bugaj\\OneDrive\\Pulpit\\JA\\JAProj_Scaling\\x64\\Release\\Scaling.dll")]
        private static unsafe extern void ScalingC(byte[] pixelsIn, byte[] pixelsOut, int h1);

        [DllImport(@"C:\Users\bugaj\OneDrive\Pulpit\JA\JAProj_Scaling\x64\Release\ScalingASM.dll")]
        private static extern int ScalingASM(byte[] source, byte[] target, int y);

        [DllImport(@"C:\Users\bugaj\OneDrive\Pulpit\JA\JAProj_Scaling\x64\Release\ScalingASM.dll")]
        private static extern void SetParametersASM(int w1, int w2, int h1, int h2);

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        private BitmapSource Bitmap2BitmapImage(Bitmap bitmap)
        {
            IntPtr hBitmap = bitmap.GetHbitmap();
            BitmapSource retval;

            try
            {
                retval = Imaging.CreateBitmapSourceFromHBitmap(
                             hBitmap,
                             IntPtr.Zero,
                             Int32Rect.Empty,
                             BitmapSizeOptions.FromEmptyOptions()
                             );
            }
            finally
            {
                DeleteObject(hBitmap);
            }

            return retval;
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void thSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            threads = (int)thSlider.Value;
            testBar.Text = threads.ToString();
        }
    }   
}
